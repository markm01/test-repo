# Demo project

A very basic demo project for DEVsheds.

## Installing DEVsheds for your own repo

To link your own repository (which may be private), use either:

Enter the following link into you browser, which will authorise the app for
your Bitbucket account and workspace. (You may be asked to login if you haven't 
done so already.)

    https://bitbucket.org/site/addons/authorize?descriptor_uri=https://app.europanb.online&redirect_uri=https://app.europanb.online


Alternatively,

1. Once you are logged in to your Bitbucket account, click on your Profile in the bottom left corner and select the workspace you want to add DEVsheds to.


2. Then click on the Settings link in the left-hand sidebar menu.


3. In Workspace Settings, click on the “Installed apps” link under “Apps and Features”.

    **Please note:** you will to enable development mode to install the app. Please ensure that the “Enable development mode” checkbox is selected.


4. Next click on the “Install app from URL” in the top-right corner of the page, which will open a popup dialog.


5. Finally, enter the DEVsheds app descriptor address: https://app.europanb.online into the App URL input box and click on Install.


With the app created, the following link will authorise the app for a selected workspace. (You may be asked to login if you haven't  done so already.)

If the app is already registered:

    https://bitbucket.org/site/addons/authorize?addon_key=bitbucket-europa-addon&redirect_uri=https://app.europanb.online


otherwise:

    https://bitbucket.org/site/addons/authorize?descriptor_uri=https://app.europanb.online&redirect_uri=https://app.europanb.online


See the [DEVsheds documentation](https://europanb.atlassian.net/wiki/spaces/DEV/pages/375455938/Installation) for further instructions.

Useful links:

* [Demo Video](https://youtu.be/jxe5GbjlBZ0)

* [DEVsheds Documentation](https://europanb.atlassian.net/wiki/spaces/DEV/pages/375390220/DEVsheds+Documentation)

* [DEVsheds website](https://www.devsheds.com/)